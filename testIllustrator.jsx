/*

if (app.documents.length > 0) {
    //@target illustrator
    //@targetengine 'main'

    //var window = new Window('tab', 'Some title');
    var window = new Window('window', 'Some title');

    var panel = window.add('panel', undefined, 'Panel title');

    panel.add('edittext', undefined, 'Default value');

    panel.add('slider', undefined, 3,0,5);
    
    var container = window;

    var group = container.add('group', undefined, 'Group title');

    //group = area_len_box.add('group', undefined, 'Title (not displayed)');

    group.orientation = 'row';

    group.closeBtn = group.add('button', undefined, 'Close', {name: 'close'});

    group.closeBtn.onClick = function () {

        container.hide();

        return false;

    };

    container.show();
}
*/
//@include "./Round Any Corner.js"
//@include "./math.js"

function AddGradient(document, pathItems, fromColor, toColor) {
    // Create a color for both ends of the gradient
    var startColor = new RGBColor();
    var endColor = new RGBColor();
    startColor.red = 0; 
    startColor.green = 100; 
    startColor.blue = 255; 
    endColor.red = 220; 
    endColor.green = 0; 
    endColor.blue = 100;

    startColor = fromColor;
    endColor = toColor;
// Create a new gradient
// A new gradient always has 2 stops
    var newGradient = document.gradients.add(); 
    newGradient.name = "NewGradient";
    newGradient.type = GradientType.LINEAR;
// Modify the first gradient stop 

    newGradient.gradientStops[0].rampPoint = 0; 
    newGradient.gradientStops[0].midPoint = 50; 
    newGradient.gradientStops[0].color = startColor;
    
// Modify the last gradient stop
    newGradient.gradientStops[1].rampPoint = 90; 
    newGradient.gradientStops[1].color = endColor;
// construct an Illustrator.GradientColor object referring to the // newly created gradient
    var colorOfGradient = new GradientColor(); 
    colorOfGradient.gradient = newGradient;
    colorOfGradient.angle = 1.7;
    colorOfGradient.hiliteAngle = 1.7;
    
// get first path item, apply new gradient as its fill  
    var topPath = pathItems; 
    topPath.filled = true;
    topPath.fillColor = colorOfGradient;

    topPath.rotate(-90, false, false, true, false, Transformation.CENTER);
}

function CreateTopFigure(size, figureGroup, borderRadius, document) {
    var offset = Array(1.5,1,1.5,3);//first left side, last bottom side
    
    var height = 12;

    var shapePath = figureGroup.pathItems.add();
    shapePath.setEntirePath(Array(
        Array(0 + offset[0], 0 + size[1] - offset[1] - height),
        Array(0 + offset[0], 0 + size[1] - offset[1]),
        Array(0 + size[0] - offset[2], 0 + size[1] - offset[1]),
        Array(0 + size[0] - offset[2], 0 + size[1] - offset[1] - height)
    ));

    shapePath.closed = true;
    shapePath.stroked = false;
    shapePath.filled = true;

    var fillColor = new RGBColor;
    fillColor.red = 248;
    fillColor.green = 238;
    fillColor.blue = 69;
    shapePath.fillColor = fillColor;

    var fillColor2 = new RGBColor;
    fillColor2.red = 248 - 90;
    fillColor2.green = 238 - 90;
    fillColor2.blue = 69 - 69;
    
    //AddGradient(document, shapePath, fillColor, fillColor2);
    
    return shapePath;
}
function CreateBottomFigure(size, figureGroup, borderRadius) {
    var offset = Array(1.5,1,1.5,3);//first left side, last bottom side

    var height = 6;

    var shapePath = figureGroup.pathItems.add();
    shapePath.setEntirePath(Array(
        Array(0 + offset[0], 0 + offset[3]),
        Array(0 + offset[0], 0 + offset[3] + height),
        Array(0 + size[0] - offset[2], 0 + offset[3] + height),
        Array(0 + size[0] - offset[2], 0 + offset[3])
    ));

    shapePath.closed = true;
    shapePath.stroked = false;
    shapePath.filled = true;

    var fillColor = new RGBColor;
    fillColor.red = 162;
    fillColor.green = 85;
    fillColor.blue = 41;
    shapePath.fillColor = fillColor;


    return shapePath;
}
function CreateCenterFigure(size, figureGroup, borderRadius) {
    var offsetTop = 5;
    var offsetBottom = 6;
    var offset = Array(1.5,1 + offsetTop,1.5,3 + offsetBottom);//first left side, last bottom side
    
    var shapePath = figureGroup.pathItems.add();
    shapePath.setEntirePath(Array(
        Array(0 + offset[0], 0 + offset[3]),
        Array(0 + offset[0], 0 + size[1] - offset[1]),
        Array(0 + size[0] - offset[2], 0 + size[1] - offset[1]),
        Array(0 + size[0] - offset[2], 0 + offset[3])
    ));

    shapePath.closed = true;
    shapePath.stroked = false;
    shapePath.filled = true;
    
    var fillColor = new RGBColor;
    fillColor.red = 237;
    fillColor.green = 196;
    fillColor.blue = 8;
    shapePath.fillColor = fillColor;

    return shapePath;
}
function CreateBackgroundFigure(size, figureGroup, borderRadius) {
    var shapePath = figureGroup.pathItems.add();
    shapePath.setEntirePath(Array(
        Array(0, 0),
        Array(0, 0 + size[1]),
        Array(0 + size[0], 0 + size[1]),
        Array(0 + size[0], 0)
    ));

    shapePath.closed = true;
    shapePath.stroked = false;
    shapePath.filled = true;

    var fillColor = new RGBColor;
    fillColor.red = 0;
    fillColor.green = 0;
    fillColor.blue = 0;
    shapePath.fillColor = fillColor;

    return shapePath;
}

function CreateLargeButton() {
    var shearAngle = -5;
    
    var documentSize = Array(280,92);

    var offsetXOnMaxHeight = GetOffsetXOnMaxHeight(documentSize, shearAngle);
    var figureSizeInitial = Array(documentSize[0]-offsetXOnMaxHeight,documentSize[1]);
    
    var mainFigureBorderRadius = 4;
    var insideFigureRightTopBorderRadius = 8;

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    var figureGroup = document.groupItems.add();

    var shape1 = CreateBackgroundFigure(figureSizeInitial, figureGroup, mainFigureBorderRadius);
    var shape2 = CreateTopFigure(figureSizeInitial, figureGroup, mainFigureBorderRadius, document);
    var shape3 = CreateBottomFigure(figureSizeInitial, figureGroup, mainFigureBorderRadius);
    var shape4 = CreateCenterFigure(figureSizeInitial, figureGroup, insideFigureRightTopBorderRadius);

    SelectAllVerticesInShape(shape1);
    SelectAllVerticesInShape(shape2);
    SelectAllVerticesInShape(shape3);
    SelectAllVerticesInShape(shape4);


        ShearShape(offsetXOnMaxHeight, document);


        app.selection = [];

        //===

        var pathPoints = shape1.pathPoints;
        pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[1].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[2].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[3].selected = PathPointSelection.ANCHORPOINT;

        pathPoints = shape2.pathPoints;
        pathPoints[1].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[2].selected = PathPointSelection.ANCHORPOINT;

        pathPoints = shape3.pathPoints;
        pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[3].selected = PathPointSelection.ANCHORPOINT;

        pathPoints = shape4.pathPoints;
        pathPoints[2].selected = PathPointSelection.ANCHORPOINT;

        roundAnyCorner(Array(shape1, shape2, shape3), mainFigureBorderRadius);
        roundAnyCorner(Array(shape4), insideFigureRightTopBorderRadius);


}

function GetPositionAroundCircleByRadius(radius, angle) {
    angle += 90;
    var radian = angle * 0.0174532925;
    var x = radius *  Math.cos(radian);
    var y = radius *  Math.sin(radian);
    return Array(x,y);
}

function GetOffsetXOnMaxHeight(size, angle) {
    var width = size[0];
    var height = size[1];

    var newPositionAroundCircle = GetPositionAroundCircleByRadius(height, angle);
    newPositionAroundCircle[0] *= 2;
    newPositionAroundCircle[1] *= 2;

    var newPosition =  checkIntersection(Array(0,0), newPositionAroundCircle, Array(0,height), Array(width*2,height));
    var offsetXOnMaxHeight = newPosition[0];
    
    return offsetXOnMaxHeight;
}

function ShearShape(offsetXOnMaxHeight, document) {
    var height = document.height;

    var len = app.selection.length;
    alert(1);// После строки `var len = app.selection.length;` нужен alert, иначе не наклоняются объекты

    for (var i=0; i<len; i++) {
        //var shape = app.selection[i];
        var group = app.selection[i];
        if (!group) {
            continue;
        }
        if (group.constructor.name == "GroupItem") {
            var len3 = group.pathItems.length;
            for (var w=0; w<len; w++) {
                var shape = group.pathItems[w];

                var pathPoints = shape.pathPoints;
                var len2 = pathPoints.length;

                for (var d=0; d<len2; d++) {
                    var point = pathPoints[d];

                    var position = point.anchor;
                    var coefByHeight = position[1] / height;
                    position[0] += coefByHeight * offsetXOnMaxHeight;
                    point.anchor = position;
                    point.leftDirection = position;
                    point.rightDirection = position;
                }
            }
        } else
        if (group.constructor.name == "PathPoints") {
            var shape = group;

            var pathPoints = shape.pathPoints;
            var len2 = pathPoints.length;

            for (var d=0; d<len2; d++) {
                var point = pathPoints[d];

                var position = point.anchor;
                var coefByHeight = position[1] / height;
                position[0] += coefByHeight * offsetXOnMaxHeight;
                point.anchor = position;
                point.leftDirection = position;
                point.rightDirection = position;
            }
        }

    }
}

function ShearShape2(angle) {
    var tm = new Matrix();
    tm.mValueA = 1;
    tm.mValueB = 0;
    tm.mValueC = Math.tan(Math.PI/180*angle);
    tm.mValueD = 1;
    tm.mValueTX = 0;
    tm.mValueTY = 0;
    
    var len = app.selection.length;
    for (var i=0; i<len; i++) {
        app.selection.transform(tm,true,true,true,true,1);
    }
}

function SelectAllVerticesInShape(shape) {
    var pathPoints = shape.pathPoints;
    var len = pathPoints.length;
    for (var i=0; i<len; i++) {
        var vertex = pathPoints[i];
        vertex.selected = PathPointSelection.ANCHORPOINT;
    }
}

main();
function main() {
    CreateLargeButton();
}
/*

if (app.documents.length > 0 && false==true) {
    //var document = app.activeDocument;
    var size = Array(268,90);

    var document = app.documents.add(null, size[0], size[1]);
    
    var figureGroup = document.groupItems.add();
    
    var mainFigureBorderRadius = 4;
    var insideFigureRightTopBorderRadius = 4;
    
    var trianglePath = figureGroup.pathItems.add();
    trianglePath.setEntirePath(Array(
        Array(0, 0),
        Array(0, 0 + size[1]),
        Array(0 + size[0], 0 + size[1]),
        Array(0 + size[0], 0)
    ));
    
    trianglePath.closed = true;
    trianglePath.stroked = false;
    trianglePath.filled = true;
    //trianglePath.strokeWidth = 3;
    
    var pathPoints = trianglePath.pathPoints;

    pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
    pathPoints[1].selected = PathPointSelection.ANCHORPOINT;
    pathPoints[2].selected = PathPointSelection.ANCHORPOINT;
    pathPoints[3].selected = PathPointSelection.ANCHORPOINT;
    //trianglePath.pathPoints[2].anchor = Array(-3 + size[0], -7 + size[1]);

    //trianglePath.pathPoints[2].pointType = PointType.SMOOTH;
    
    
    //var captionText = figureGroup.textFrames.add();
    //captionText.position = Array(100, 150);
    //captionText.textRange.size = 48;
    //captionText.contents = "A triangle";
     
    
    var fillColor = new RGBColor;
    fillColor.red = 255;
    fillColor.green = 0;
    fillColor.blue = 0;
    //captionText.characters.fillColor = fillColor;
    
    roundAnyCorner(Array(trianglePath));

    var objForSave = document;
    ExportFileToPNG24('/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/TinyTanks/tinytanks.server/file', objForSave);
}
 */

function ExportFileToPNG24(path, obj) {
    var exportOptions = new ExportOptionsPNG24();
    var type = ExportType.PNG24;
    var fileSpec = new File(path);
    exportOptions.antiAliasing = false;
    exportOptions.transparency = false;
    exportOptions.saveAsHTML = true;
    obj.exportFile(fileSpec, type, exportOptions);
}