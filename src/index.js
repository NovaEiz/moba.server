const express = require('express');
const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);

let app = express();

const matchmaking = require('./matchmaking')(app);

const UserModel = require('./models/UserModel')(AutoIncrement);
const HeroModel = require('./models/HeroModel')(AutoIncrement);
const GameModel = require('./models/GameModel')(AutoIncrement);

matchmaking.SetUserModel(UserModel);
matchmaking.SetHeroModel(HeroModel);
matchmaking.SetGameModel(GameModel);

async function Start() {
    try {
        let password = "1375613_Server_love";
        let dbName = "starfights";
        let connectUri = "mongodb+srv://novaeiz:" + password + "@cluster0.puolb.gcp.mongodb.net/" + dbName + "?retryWrites=true&w=majority";
        
        console.log("connectUri = " + connectUri);
        
        await mongoose.connect(
            connectUri,
            {
                useNewUrlParser: true,
                useFindAndModify: false
            }
        );
        
        let port = process.env.PORT || 9999;

        let server = app.listen(port, () => {
            console.log("server has been started on port = " + port);
        });

        server.setTimeout(2 * 60 * 1000);//Устанавливает время, после которого будет остановлен запрос на клиенте, если сервер не отправил ответ
    } catch (e) {
        console.log(e);
        console.error("Не подключается к MongoDB базе https://cloud.mongodb.com/. Возможно на этом сайте не указан этот используемый IP.");
    }
}

Start();

//Admin
//sEExk9i4u@Drwx7

/*

#Nighday.CoreBootstrap
{

CoreBootstrap - Это основа.

В проекте используются:
- Облачная mongodb база.
- Облачный ubuntu сервер.

Что делает CoreBootstrap:

- Проверка версии на актуальность. Через сервер авторизации (AuthServer).
- Если игра онлайн, то если надо обновить сам билд, позволить играть на старом сервере,
иначе если надо обновить бандлы или настройки из таблиц, то обязать клиента это сделать.
- После этого запросить у сервера авторизации(AuthServer) данные для подключения к главному серверу (MainServer).

MainServer REST API.
MainServer предназначен для получения из базы всех данных для игры.


}

#Nighday.Matchmaking
{
    #Nighday.Matchmaking.Api
    {
Matchmaking REST API.
Matchmaking предназначен для создания сервера игры (GameServer) и передачи игрокам данных для подключения к GameServer.

    }
    #Nighday.Matchmaking.Client
    {
        
    }
    #Nighday.Matchmaking.GameServer
    {
        Это часть 
    }
    #Nighday.Matchmaking.ServerGameManager
    {
        Это часть 
    }
}

 */