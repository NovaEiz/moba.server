const bodyParser = require('body-parser');
const request = require('request-promise');

let jsonParser = bodyParser.json();

async function GetExternalIP(callback) {
    let res = await request.post({
        uri: "https://jsonip.com/",
        method: "GET",
        json: true
    });
    return res["ip"];
}

module.exports =
    (app, sharedData) => {

        app.post('/', jsonParser, async (req, res) => {
            let port = req.body.port;
            let ip = req.hostname;
            if (ip == "127.0.0.1" && !process.env.IS_ALPHA) {
                ip = await GetExternalIP();
            }
            
            let gsm = await sharedData.AddOrReplaceGameServerManager(
                ip, port
            );

            res.send({
                code: 1
            });
        });
        
    };
