let express = require('express');
let sleep = require('sleep-promise');
const bodyParser = require('body-parser');
const request = require('request-promise');

let jsonParser = bodyParser.json();

let gsmList = {};

let nextGsmId = 1;
function GenerateGsmId() {
    let id = nextGsmId;
    nextGsmId++;
    return id;
}

let sharedData = {
    AddOrReplaceGameServerManager: async function (ip, port) {
        let gsm = {
            id: GenerateGsmId(),
            ip,
            port,
            GetUrl: function () {
                let url = "http://" + ip + ":" + port;
                return url;
            }
        };
        
        gsmList[gsm.id] = gsm;
    },
    GetGameServerManager: function (params) {
        for (let i in gsmList) {
            let gsm = gsmList[i];
            return gsm;
        }
        return null;
    },
    RemoveGameServerManager: function (gsm) {
        for (let i in gsmList) {
            let gsmItem = gsmList[i];
            if (gsm.id == gsmItem.id) {
                delete gsmList[i];
                return;
            }
        }
    },
};
const timeout = ms => new Promise(res => setTimeout(res, ms));

let globalSharedData = {
    CreateGame: async function (gameModeData, playersData) {
        playersData = JSON.parse(JSON.stringify(playersData));
        
        let gsmData = sharedData.GetGameServerManager();
        
        if (!gsmData) {
            console.error("Не могу создать игру. Нет активных GameServerManager'ов! Жду подключения GameServerManager'a.");
            return null;
        }
        
        // Создать игру
        let url = gsmData.GetUrl() + "/games";

        let res = null;
        
        try {
            res = await request.post({
                uri: url,
                method: "POST",
                json: true,
                body: null,
                timeout: 5000
            })
            .catch((e) => {
                console.error("Не могу создать игру. GameServerManager выдал неизвестную ошибку во время запроса! Повторю попытку позже, выбрав другой GameServerManager.");
                res = null;
            });
        } catch (e) {
            res = null;
            console.error("Не могу создать игру. GameServerManager отключился! Повторю попытку позже, выбрав другой GameServerManager.");
        }

        if (!res) {
            sharedData.RemoveGameServerManager(gsmData);
            return null;
        }
        
        if (res.code == 404) {
            sharedData.RemoveGameServerManager(gsmData);
            console.error("Не могу создать игру. Нет свободных портов на GameServerManager'e! Повторю попытку позже.");
            return null;
        }
        console.error("Создал игру.");

        // Вставить gameData в базу данных и получить _id новой игры
        let game = new sharedData.models.GameModel({});
        await game.save();
        console.error("Создал игру 2.");

        let gameContacts = {};
        gameContacts.id = game._id;
        gameContacts.host = gsmData.ip;
        gameContacts.port = res.port;
        console.error("Создал игру 3.");

        game.gameModeName = gameModeData["gameModeName"];
        game.gameModeData = gameModeData;
        game.gameContacts = gameContacts;
        game.gameData = {};
        console.error("Создал игру 4.");

        let gameModeTeamType = gameModeData["gameModeTeamType"];
        console.error("Создал игру 5 gameModeTeamType = " + gameModeTeamType);

        if (gameModeTeamType == 0) { // EveryManForHimself
            game.gameData.players = playersData;
        } else  if (gameModeTeamType == 1) { // InTeam
            let teams = [];

            let playersAmountInTeam = gameModeData["playersInTeamAmount"];
            
            let teamsAmount = gameModeData["playersAmount"] / playersAmountInTeam;
            
            for (let t=0; t<teamsAmount; t++) {
                let playersDataInTeam = playersData.splice(0, playersAmountInTeam);

                let team = {
                    id: t+1,
                    players: playersDataInTeam
                };
                teams.push(team);
            }

            game.gameData.teams = teams;
        }

        // Отправить игре данные options и данные об игроках

        //url = gsmData.GetUrl() + "/games/" + gamePort + "/data";
        url = gsmData.GetUrl() + "/games/data";
        
        let tryCount = 5;
        
        let isDone = false;
        while (!isDone && tryCount > 0) {
            try {
                let res_ = await request.put({
                    uri: url,
                    json: true,
                    body: {
                        gameData: game,
                        gameContacts: game.gameContacts,
                        gameModeName: game.gameModeName
                    }
                });
                isDone = true;
            } catch (e) {
                tryCount--;
                console.error("Не смог отправить gameData на GMS сервер. Делаю еще `" + tryCount + "` попыток и удаляю этот GSM из списка.");
            }
        }
        if (!isDone) {
            return null;
        }

        await game.save();

        return game;
    }
};

module.exports =
    (app, models) => {
        sharedData.models = models;

        let router = express.Router();

        require('./post')(router, sharedData);

        app.use('/gsm', router);
        
        return globalSharedData;
    };