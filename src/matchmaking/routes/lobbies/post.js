const bodyParser = require('body-parser');
let jsonParser = bodyParser.json();

module.exports =
    (app, sharedData) => {
        // Post. Добавление лобби в этом случае означает что игрок делает запрос на вход в поиск игры по options
        app.post('/', sharedData.jsonParser, async (req, res) => {
            let userId = req.body.userId;
            let gameModeDataJson = req.body.gameModeDataJson;

            let lobby = sharedData.GetOrCreateLobby(gameModeDataJson);

            let sendData = {
                code: 1,
                data: lobby.data
            };

            let userData = await sharedData.models.UserModel.findOne({_id: userId});

            let player = {
                _id: userId,
                name: userData.name,
                selectedHeroId: userData["selectedHeroId"]
            };
            lobby.AddPlayer(player);
            
            res.send(sendData);
        });
        app.put('/:lobby_id/leave/', sharedData.jsonParser, (req, res) => {
            let userId = req.body.userId;
            let lobbyId = req.params.lobby_id;

            let lobby = sharedData.GetLobbyById(lobbyId);
            if (!lobby) {
                res.send({
                    "code": 404
                });
                return;
            }

            let cancelResult = lobby.PlayerLeft(userId);

            if (cancelResult) {
                res.send({
                    "code": 1
                });
            } else {
                res.send({
                    "code": 405
                });
            }

        });
        app.post('/:lobby_id/accept/', sharedData.jsonParser, (req, res) => {
            let userId = req.body.userId;
            let lobbyId = req.params.lobby_id;

            let lobby = sharedData.GetLobbyById(lobbyId);
            if (!lobby) {
                res.send({
                    "code": 404
                });
                return;
            }

            let acceptResult = lobby.PlayerAccept(userId);
            
            if (acceptResult) {
                res.send({
                    "code": 1
                });
            } else {
                res.send({
                    "code": 405
                });
            }

        });

        // Юзер делает запрос на ожидание. Сегвег добавляет юзера в лист на ожидании. 
        // Чтобы когда игра будет готова, или отменена, то сообщить об этом юзеру
        // Если выходит время ожидания запроса (30 секунд), то сервер отправляет ошибку с кодом, 
        // по которому клиент повторяет этот запрос
        app.post('/:lobby_id/wait', jsonParser, (req, res) => {
            // Запрос продолжать только для игроков, которые в этом лобби

            let userId = req.body.userId;
            let lobbyId = req.params.lobby_id;

            let lobby = sharedData.GetLobbyById(lobbyId);
            if (!lobby) {
                res.send({
                    result: false,
                    errorCode: 404,
                    error: "/wait error = "
                });
                return;
            }
            let player = lobby.GetPlayerById(userId);
            if (!player) {
                res.send({
                    result: false,
                    errorCode: 404,
                    error: "/wait 2 error = "
                });
                return;
            }

            lobby.SetLastActivityTimeForPlayer(player);
            
            let response = res;

            let timeout = null;
            let interval = null;
            let sendForNewRequest = (responseData) => {
                try {
                    if (!res) {
                        return;
                    }
                    res.send(responseData);
                    lobby.RemoveUserOnWait(userId);

                    lobby.SetLastActivityTimeForPlayer(player);
                } catch (e) {
                    console.error("Error = " +e);
                }
            };

            let action = (responseData) => {
                clearTimeout(timeout);
                clearInterval(interval);
                sendForNewRequest(responseData);
            };

            interval = setInterval(() => {
                if (player.dataToSend) {
                    action(player.dataToSend);
                    player.dataToSend = null;
                }
            }, 100);

            timeout = res.setTimeout(10*1000, () => {
                action({
                    result: false,
                    errorCode: 1,
                    error: "/wait error = "
                });
            });

            lobby.AddUserOnWait(userId, action);
        });

    };
