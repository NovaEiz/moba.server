let express = require('express');
let sleep = require('sleep-promise');
const bodyParser = require('body-parser');
let jsonParser = bodyParser.json();
/*
status:

0 - default
1 - Отправлено событие GameFound
2 - Игрок принял игру
3 - Отправлено 
 
 */

function Player() {
    this._id = 0;
    this.status = 0;
}

let _currentLobbyId = 1;
function GetNextLobbyId() {
    return _currentLobbyId++;
}

function CreateLobby(gameModeDataJson) {
    let gameModeData = JSON.parse(gameModeDataJson);

    let lobby = {
        data: {
            lobbyId: GetNextLobbyId(),
            gameModeData: gameModeData,
            isFilled: false,
        },
        gameModeDataJson: gameModeDataJson,
        players: [],
        usersOnWait: {},
        playersAmount: gameModeData["playersAmount"],
        isReady: false,
        OnDestroy: () => {},
        /// ===
        /// В объекте player должно быть поле userId
        /// ===
        AddPlayer: function (player) {
            this.SetLastActivityTimeForPlayer(player);
            this.players.push(player);
            if (this.players.length == this.playersAmount) {
                this.Filled();
            }
        },
        SetLastActivityTimeForPlayer: function (player) {
            player.lastActivityTime = Date.now();

            let sec = 115;

            setTimeout(() => {
                if (Date.now() - player.lastActivityTime >= sec * 1000 - 100) {
                    lobby.RemovePlayerById(player._id);
                }
            }, sec * 1000 + 100);
        },
        RemovePlayerById: function (userId) {
            for (let i in this.players) {
                if (this.players[i]._id == userId) {
                    this.players.splice(i, 1);
                    return true;
                }
            }
            return false;
        },
        Filled: function() {
            this.data.isFilled = true;
            this.SendMessage({
                code: 55,
                data: this.data
            });
            this.WaitForReadiness();
        },
        PlayerLeft: function(userId) {
            if (this.isReady || this.data.isFilled) {
                return false;
            }
            let len = this.players.length;
            for (let i=0; i<len; i++) {
                if (this.players[i]._id == userId) {
                    delete this.players[i];
                    this.players.splice(i, 1);
                    return true;
                }
            }
            return false;
        },
        PlayerAccept: function (userId) {
            let isReady = true;
            let res = false;
            let len = this.players.length;
            for (let i=0; i<len; i++) {
                if (this.players[i]._id == userId) {
                    res = true;
                    this.players[i].accepted = true;
                }
                if (!this.players[i].accepted) {
                    isReady = false;
                }
            }
            if (isReady) {
                this.isReady = true;
            }
            return res;
        },
        WaitForReadiness: async function () {
            let maximumWaitingTime = 30 * 1000;
            let startTimeReadiness = Date.now();
            while (!this.isReady) {
                // Если истекло время ожидания для принятия игры всеми игроками, то
                // Удалить игроков которые не приняли, и продолжить поиск
                if (Date.now() - startTimeReadiness > maximumWaitingTime) {
                    this.ContinueSearch();
                    return;
                }
                await sleep(50);
            }

            let playersData = [];
            for (let i in this.players) {
                
                playersData.push({
                    _id: this.players[i]._id,
                    data: {
                        name: this.players[i].name,
                        selectedHeroId: this.players[i].selectedHeroId
                    }
                });
            }
            
            let gameModeData = this.data.gameModeData;
            // Создать игру
            let gameData = await sharedData.CreateGame(gameModeData, playersData);

            if (!gameData) {
                await sleep(1000);
                this.WaitForReadiness();
                return;
            }
            
            
            // Отправить игрокам данные об игре
            this.SendMessage({
                code: 56,
                gameModeName: gameData.gameModeName, // В gameData.gameModeData уже лежит правильный gameModeName
                gameContacts: gameData.gameContacts
            });

            // Записать игрокам в базе id этой новой игры
            for (let index in playersData) {
                let playerData = playersData[index];

                let userData = await sharedData.models.UserModel.findOne({_id: playerData._id});

                userData.activeGame = {
                    _id: gameData._id
                };
                await userData.save();
            }
            
            // Завершить(удалить) этот лобби
            this.OnDestroy();
        },
        ContinueSearch: function () {
            for (let i in this.players) {
                if (!this.players.accepted) {
                    this.players.splice(i, 1);
                    i--;
                    continue;
                }
                this.players.accepted = undefined;
            }

            this.data.isFilled = false;

            this.SendMessage({
                code: 5
            });
        },
        AddUserOnWait: function (userId, action) {
            this.usersOnWait[userId] = action;


        },
        RemoveUserOnWait: function (userId) {
            delete this.usersOnWait[userId];
        },
        SendMessage: function (data) {
            for (let key in this.players) {
                this.players[key].dataToSend = data;
            }
        },
        GetPlayerById: function (userId) {
            for (let key in this.players) {
                if (this.players[key]._id == userId) {
                    return this.players[key];
                }
            }
            return null;
        },
    };

    return lobby;
}

let sharedData = {
    lobbies: {},
    jsonParser,
    CreateGame: null,
    SetCreateGameAction: function (createGameAction) {
        this.CreateGame = createGameAction;
    },
    CreateLobby: CreateLobby,
    GetFreeLobbyByOptions: function(gameModeDataJson) {
        for (let index in sharedData.lobbies) {
            let lobby = sharedData.lobbies[index];
            if (lobby.data.isFilled) {
                continue;
            }
            if (lobby.gameModeDataJson == gameModeDataJson) {
                return lobby;
            }
        }
    },
    GetOrCreateLobby: function (gameModeDataJson) {
        let lobby = null;
        lobby = sharedData.GetFreeLobbyByOptions(gameModeDataJson);
        if (!lobby) {
            lobby = this.CreateLobby(gameModeDataJson);
            sharedData.lobbies[lobby.data.lobbyId] = lobby;
            lobby.OnDestroy = () => {
                delete sharedData.lobbies[lobby.data.lobbyId];
            };
        }
        return lobby;
    },
    GetLobby: function (gameModeData) {
        let gameModeDataJson = JSON.stringify(gameModeData);
        let lobby = sharedData.lobbies[gameModeDataJson];
        return lobby;
    },
    GetLobbyById: function (lobbyId) {
        for (let key in this.lobbies) {
            let lobby = sharedData.lobbies[key];

            if (lobby.data.lobbyId == lobbyId) {
                return lobby;
            }
        }

        return null;
    }
};

module.exports =
    (app, models) => {
    
        sharedData.models = models;
    
        let router = express.Router();
        
        require('./get')(router, sharedData);
        require('./post')(router, sharedData);

        app.use('/lobbies', router);

        return sharedData;
    };