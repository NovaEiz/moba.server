let express = require('express');
let sleep = require('sleep-promise');
const bodyParser = require('body-parser');
const request = require('request-promise');

let jsonParser = bodyParser.json();

let sharedData = {

};

let globalSharedData = {
    
};

module.exports =
    (app, models) => {
        sharedData.models = models;

        let router = express.Router();

        require('./get')(router, sharedData);
        require('./post')(router, sharedData);

        app.use('/users', router);
        
        return globalSharedData;
    };