const bodyParser = require('body-parser');
let jsonParser = bodyParser.json();

module.exports =
    (app, sharedData) => {

        app.post('/:user_id/selectedHero/:selected_hero_id', jsonParser, async (req, res) => {
            let userId = req.params["user_id"];
            let selectedHeroId = req.params["selected_hero_id"];

            let userData = await sharedData.models.UserModel.findOne({_id: userId});
            userData.selectedHeroId = selectedHeroId;
            userData.save();

            res.send({
                ok: true
            });
        });
        
        app.post('/:user_id/changeName/:new_name', jsonParser, async (req, res) => {
            let userId = req.params["user_id"];
            let newName = req.params["new_name"];

            let userData = await sharedData.models.UserModel.findOne({_id: userId});
            userData.name = newName;
            userData.save();

            res.send({
                ok: true,
                name: newName
            });
        });

    };