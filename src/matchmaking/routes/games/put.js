const bodyParser = require('body-parser');
const request = require('request-promise');

let jsonParser = bodyParser.json();

module.exports =
    (app, sharedData) => {

        app.put('/reconnect', jsonParser, async (req, res) => {
            let userId = req.body.userId;

            let userData = await sharedData.models.UserModel.findOne({_id: userId});
            
            if (!userData.activeGame) {
                res.send({
                    code: 403
                });
                return;
            }
            
            let gameId = userData.activeGame._id;

            let gameData = await sharedData.models.GameModel.findOne({_id: gameId});
            if (!gameData) {
                res.send({
                    code: 404
                });
                return;
            }

            res.send({
                code: 1,
                gameData: gameData,
                gameContacts: gameData.gameContacts,
                gameModeName: gameData.gameModeName
            });
        });
        app.put('/leave', jsonParser, async (req, res) => {
            let userId = req.body.userId;

            let userData = await sharedData.models.UserModel.findOne({_id: userId});

            if (!userData.activeGame) {
                res.send({
                    code: 403
                });
                return;
            }

            let gameId = userData.activeGame._id;

            let gameData = await sharedData.models.GameModel.findOne({_id: gameId});

            if (!gameData) {
                res.send({
                    code: 404
                });
                return;
            }

            for (let index in gameData.playersData) {
                let playerData = gameData.playersData[index];
                if (playerData._id == userId) {

                    playerData.leftTheGame = true;
                    await gameData.save();

                    res.send({
                        code: 1
                    });
                    return;
                }
            }
            
            res.send({
                code: 405
            });
        });

    };
