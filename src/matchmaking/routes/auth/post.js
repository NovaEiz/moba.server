const bodyParser = require('body-parser');
const fs = require('fs');

let jsonParser = bodyParser.json();

module.exports =
    (app, sharedData) => {
        let serverBuildVersion = process.env.buildVersion;

        /*
        if (!serverBuildVersion) {
            let npm_lifecycle_script = process.env["npm_lifecycle_script"];
            let beginArgsIndex = npm_lifecycle_script.indexOf('"');
            let argsStr = npm_lifecycle_script.substring(beginArgsIndex+1, npm_lifecycle_script.length-1);
            let argsStrSplit = argsStr.split('=');

            args[argsStrSplit[0]] = argsStrSplit[1];
            serverBuildVersion = args.buildVersion;
        }
        */

        if (!serverBuildVersion) {
            let args = {};
            let npm_config_argv = JSON.parse(process.env["npm_config_argv"]);

            let remainArgs = npm_config_argv["remain"];
            for (let index in remainArgs) {
                let arg = remainArgs[index];
                let argStrSplit = arg.split('=');

                args[argStrSplit[0]] = argStrSplit[1];
            }
            serverBuildVersion = args.buildVersion;
        }
        
        if (!serverBuildVersion) {
            return;
        }
        
        app.post('/', jsonParser, async (req, res) => {
            let deviceId = req.body.deviceId;
            let userBuildVersion = req.body.buildVersion;
            
            if (serverBuildVersion != userBuildVersion) {
                res.send({
                    code: 2
                });
                return;
            }
            let userData = await sharedData.models.UserModel.findOne({deviceId: deviceId});
            if (!userData || userData.length == 0) {
                userData = new sharedData.models.UserModel({deviceId: deviceId, name: deviceId});
                await userData.save();
            }

            res.send({
                code: 1,
                userData: userData
            });
        });
        
    };
