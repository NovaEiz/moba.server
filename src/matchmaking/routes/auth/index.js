let express = require('express');
let sleep = require('sleep-promise');
const bodyParser = require('body-parser');

let jsonParser = bodyParser.json();

let sharedData = {
    
};

module.exports =
    (app, models) => {
        sharedData.models = models;

        let router = express.Router();

        require('./post')(router, sharedData);

        app.use('/auth', router);
        
    };