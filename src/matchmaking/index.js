let config = {
    port: 9999,
    gamesTableName: "games",
    mongoDatabaseAuthUrl: ""
};

let models = {
    UserModel: null,
    GameModel: null
};

function SetUserModel(value) {
    models.UserModel = value;
    //gameSearchController.SetUserModel(UserModel);
}
function SetHeroModel(value) {
    models.HeroModel = value;
    //gameSearchController.SetHeroModel(HeroModel);
}
function SetGameModel(value) {
    models.GameModel = value;
    //gameSearchController.SetGameModel(GameModel);
}

module.exports = (app) => {

    let gsmSharedData = require('./routes/gsm')(app, models);
    let lobbiesSharedData = require('./routes/lobbies')(app, models);
    lobbiesSharedData.SetCreateGameAction(gsmSharedData.CreateGame);
    let gamesSharedData = require('./routes/games')(app, models);
    let authSharedData = require('./routes/auth')(app, models);
    let usersSharedData = require('./routes/users')(app, models);

    return {
        config: config,
        SetUserModel,
        SetHeroModel,
        SetGameModel
    };
};



