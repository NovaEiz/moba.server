const {Schema, model} = require('mongoose');

const UserModel = new Schema({
    _id: false,
    deviceId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    selectedHeroId: {
        type: Number,
        default: 1
    },
    activeGame: {
        _id: {
            type: Number,
            default: 0
        },
        disconnectedFromTheGame: {
            type: Boolean,
            default: false
        }
    },
    heroes: {
        type: Array,
        default: []
    }
});

module.exports = (AutoIncrement) => {
    UserModel.plugin(AutoIncrement, {id: 'userId', inc_field: '_id'});


    return model('users', UserModel);
};
