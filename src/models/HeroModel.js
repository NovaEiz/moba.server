const {Schema, model} = require('mongoose');

const HeroSchema = new Schema({
    _id: false,
    name: {
        type: String,
        required: true
    },
    level: {
        type: Number,
        default: 1
    }
});

module.exports =  (AutoIncrement) => {
    HeroSchema.plugin(AutoIncrement, {id: 'heroId', inc_field: '_id'});
    
    return  model('heroes', HeroSchema);
};