const {Schema, model} = require('mongoose');

const GameSchema = new Schema({
    _id: false,
    gameContacts: {
        id: Number,
        host: String,
        port: Number
    },
    gameModeData: Object,
    gameModeName: String,
    gameData: Object
});

module.exports =  (AutoIncrement) => {
    GameSchema.plugin(AutoIncrement, {id: 'gameId', inc_field: '_id'});
    
    return  model('games', GameSchema);
};