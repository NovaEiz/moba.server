Подключение по shh:

ssh 35.228.39.198

Запуск Головного сервера:

buildVersion={version} npm run alpha
где {version} это версия клиента.
Вместо альфа указывать можно beta и release. (должно быть)

ssh 35.228.39.198
cd projects/moba.server/
pm2 start npm -- run alpha buildVersion=0.9


(Важно!)
Так через pm2 запускается beta из scripts из package.json проекта node.js:
pm2 start npm -- run beta

Переход в папку головного сервера:

cd projects/moba.server/
cd projects/StarFights/starfights.server

Переход в папку игрового сервера:

cd projects/moba.gameserver/


Загрузка репозитория сервера:

ssh 35.228.39.198
cd projects
git clone https://gitlab.com/novaeiz.games/moba/moba.server.git
cd moba.server/
npm install

(npm install для установки папки node_modules)

cd projects/moba.server/


Без pm2 параметры можно передавать 2 способами:
- buildVersion=0.9 npm run alpha



npm run alpha buildVersion=0.9

pm2 start npm -- run alpha buildVersion=0.9